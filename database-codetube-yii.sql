-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: dbyii
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` text,
  `filepath` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (4,'unta.jpg','uploads/1fc78cb0aba792302f1c0dc7195c1c76.jpg'),(6,'free-wallpaper-34.jpg','uploads/811822119d198ad943454daf234b090f.jpg'),(7,'haya.jpg','uploads/7e09f4be5df1e5ad108d0e10bcb365e7.jpg'),(9,'Red-Flower-Garden-Wallpapers.jpg','uploads/d10ec2df191440737005d80d0e6a3480.jpg');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1528609085),('m180610_053743_create_new_user_table',1528609089),('m180617_061959_create_media_table',1529216463);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `new_user`
--

DROP TABLE IF EXISTS `new_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `new_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `authKey` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `new_user`
--

LOCK TABLES `new_user` WRITE;
/*!40000 ALTER TABLE `new_user` DISABLE KEYS */;
INSERT INTO `new_user` VALUES (4,'tedirghazali','tedirghazali@gmail.com','$argon2i$v=19$m=1024,t=2,p=2$aFhHWk1CVVNXL2RvS0FvTw$PTQFup7pZdprKqigdsub2Zb2+anItZCLRDHCkGWBxlM','ebcb832d94a8d54c6024facb685e573c','$2y$10$wTIYWWGfj.a7QAlr2TEfaeHeebkVLlOIBX7kTp/vcrz0HxJWP69nq'),(5,'bukhari','bukhari.zulkifli@gmail.com','$argon2i$v=19$m=1024,t=2,p=2$MzhaRFFlUHJCM1VKeVdnZw$EtdqYt+1bnpE8XJ4NB6eZDSL5HGS71cHXDG58AmJ9XU','d30ebd2e5cd2cbb6ca679e088f5b1bd4','$2y$10$vSoPEwfOOuAB2yEPJ2UGpOhUuJiRmq5BbiTyhkXTtEc6rHCa1MuNa'),(6,'boyhaki','boyhaki@gmail.com','$argon2i$v=19$m=1024,t=2,p=2$OGdmaGkudzJzdU4xTi5ibg$PXTR27U2PTP5QMauyUJVS7feiQbM3LOY7gfm5D7Npew','a48b5eb9036d2c7c788c388f7791b1b1','$2y$10$c2VGT.NjCAaWXlxDjvoP7OP5DIgvBrYSbffFD9Lc3kgy7OzCUxOhG'),(7,'murad','murad@gmail.com','$argon2i$v=19$m=1024,t=2,p=2$bjhoanUzNzYwcEkzODdUZw$didEPuGDh0mSSw1ssUjrSdEv0kSTYo445yPMNhmmtJc','5db1603e35ff21431f401d564b551f70','$2y$10$XLllYdn.m1yGTVVGaqGcmeipcixCJXefwuZfpLd.pGGzXv4XZkXNK');
/*!40000 ALTER TABLE `new_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,'Belajar Framework','Belajar Framework',NULL,NULL),(2,'Belajar Yii','Belajar Yii 2','2018-06-08 13:50:43','2018-06-08 13:50:43');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-20 10:25:02
